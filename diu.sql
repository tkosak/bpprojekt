--Insert za tablicu DRIVERS - podaci su stvarni
----------------------------------------------
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Tomislav','Kosak','AM');
---------------------------------------------
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Uros','Jelenko','Pro');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Denis','Medvesek','PRO');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Gasper','Mrak','PRO');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Filip','Borovec','PRO');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Igor','Krstinic','PRO');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Simon','Terbovsek','PRO');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Matevz','Bas','PRO');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Primoz','Fidrisek','PRO');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Damir','Hrusevar','PRO');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Danilo','Skela','PRO');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Mitja','Miladinovic','PRO');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Rok','Kopse','PRO');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Edo','Kopse','PRO');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Milos','Milutinovic','AM');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Sanel','Tadic','AM');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Romano','Berinic','PRO');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Domagoj','Drvenkar','AM');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Kevin','Tutic','PRO');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Danijel','Majer','PRO');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Zlatko','Pavlovic','PRO');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Domagoj','Tonzetic','AM');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Amar','Dizdarevic','PRO');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Zvonimir','Kranjcec','AM');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Darko','Balatinac','AM');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Irfan','Omerasevic','AM');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Mihael','Matika','PRO');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Gordan','Volarevic','AM');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Josip','Brlas','PRO');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Hrvoje','Pindric','PRO');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Ivan','Jakelic','AM');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Matej','Miksic','PRO');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Vanja','Modrusan','PRO');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Igor','Knapic','AM');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Kruno','Grgic','AM');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Ivo','Zoricic','AM');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Tomaz','Zagar','AM');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Goran','Radosevic','AM');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Ivan','Zovko','AM');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Mladen','Durlen','AM');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Ivan','Vugrinec','AM');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Milos','Ilic','AM');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Hrvoje','Dobrovic','AM');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Emir','Celebic','AM');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Damir','Vranic','AM');
    
INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Jason','Moore','AM');

INSERT INTO DRIVERS
    (name,surname,class)
VALUES 
    ('Sandi','Camdzic','AM');
---------------------------------------------
 --UPDATE tablice DRIVERS
---------------------------------------------  
select * from DRIVERS;
UPDATE DRIVERS
SET CLASS = 'PRO'
WHERE
CLASS = 'Pro';

UPDATE DRIVERS
SET IME = 'Tomislav'
WHERE
SURNAME = 'Moore';

UPDATE DRIVERS
SET SURNAME = 'Profic'
WHERE
IME = 'Kruno';

DELETE FROM DRIVERS
WHERE 
IME = 'Tomislav';


---------------------------------------------
 --INSERTOVI u tablicu Teams
---------------------------------------------
INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('SRT MJ','McLaren 720S GT3 2019',2,3);
-------------------------------------
-- brisanje podataka zbog izmjene datatype za driver1 i driver2
/*Delete from Teams 
where driver1 = 2;
Delete from Teams 
where driver2 = 3;
----------------------------------------------
select * From Teams;
select * from drivers;*/
-------------------------------------
INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('SRT M M','BMW M6 GT3 2017',4,8);
    
INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('Piquentum forces','Porsche 911 II GT3 R 2019',6,16);  
    
INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('DC-Gun Racing Team','Ferrari 488 GT3 EVO 2020',7,12);
    
INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('SRT Bentley GT3','Bentley Continental GT3 2018',7,11);
    
INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('Pirate Racing','Porsche 911 II GT3 R 2019',5,17);
    
INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('Kopse Racing Team','Audi R8 LMS Evo 2019',13,14);

INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('D and D racing','Ferrari 488 GT3 EVO 2020',10,20);
    
INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('Apex predators','Mercedes-AMG GT3 2020',18,21);

INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('..::PiS::..CroRacing A','AMR V8 Vantage 2019',25,28);
    
INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('..::PiS::..CroRacing B','AMR V8 Vantage 2019',35,40);
    
INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('Team Beli Potok','Ferrari 488 GT3 EVO 2020',15,42);
    
INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('AC Corse team','Ferrari 488 GT3 EVO 2020',26,31);
    
INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('Baby Godzilla','Lamborghini Huracan GT3 Evo 2019',19,43);
    
INSERT INTO TEAMS
    (name,car,driver1,driver2)
VALUES
    ('Kamikaza Racing Team','McLaren 720S GT3 2019',24,36);
    
----------------------------------------
--UPDATE
----------------------------------------

UPDATE TEAMS
SET CAR = 'MERCEDES-AMG GT3 2020'
WHERE ID = 11;

UPDATE TEAMS
SET CAR = 'AMR V8 Vantage 2019'
WHERE ID = 11;

UPDATE TEAMS
SET CAR = 'MERCEDES-AMG GT3 2020'
WHERE NAME ='Pirate Racing';

DELETE FROM TEAMS
WHERE DRIVER1 = 7;


----------------------------------------
--Insertovi u tablicu Tracks
----------------------------------------
INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Barcelona','Spain',4655);

INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Brands Hatch','United Kingdom',4207);

INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Donington Park','United Kingdom',4020);

INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Hungaroring','Hungary',4381);

INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Kyalami','South Africa',4104);
    
INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Laguna Seca','USA',3610);
    
INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Misano','Italy',4226);
    
INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Monza','Italy',5793);

INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Mount Panorama','Australia',6213);

INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Nurburgring','Germany',4551);
    
INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Oulton Park','United Kingdom',4307);

INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Paul Ricard','France',5842);
    
INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Silverstone','United Kingdom',5901);
    
INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Snetterton','United Kingdom',4779);
    
INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Spa-Francorchamps','Belgium',7004);
    
INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Suzuka','Japan',5807);

INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Zandvoort','Netherlands',4193);

INSERT INTO TRACKS
    (name,country,lenght)
VALUES
    ('Zolder','Belgium',4220);
-------------------------------------
-- UPDATE
-------------------------------------
UPDATE TRACKS
SET LENGHT = LENGHT + 500
WHERE LENGHT >4000;

UPDATE TRACKS
SET LENGHT = LENGHT - 500
WHERE LENGHT >4000;

UPDATE TRACKS
SET COUNTRY = 'UK'  
WHERE COUNTRY = 'United Kingdom';


DELETE FROM TRACKS
WHERE LENGHT <=6000;
--select * from TRACKS;

-------------------------------------
-- Insertovi u tablicu teams points
-------------------------------------
INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (554,1);
    
INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (385,2);

INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (304,3); 
 ----   
INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (302,4); 

INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (295,5);

INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (290,6);

INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (266,7);

INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (224,8);
    
INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (169,9);
    
INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (120,10);

INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (112,11);
    
INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (96,12);
    
INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (83,13);

INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (78,14);

INSERT INTO TEAMSPOINTS
    (points,teamsid)
VALUES
    (76,15);
-------------------------------------
--UPDATE
-------------------------------------
UPDATE TEAMSPOINTS
SET POINTS = POINTS + 50
WHERE POINTS <=50;

UPDATE TEAMSPOINTS
SET POINTS = POINTS + 10
WHERE POINTS >=120;

UPDATE TEAMSPOINTS
SET POINTS = POINTS + 100
WHERE POINTS <75;


--
DELETE FROM TEAMSPOINTS
WHERE POINTS <80;

DELETE FROM TEAMSPOINTS
WHERE POINTS <=77;

--select * from TEAMSPOINTS;
-------------------------------------
-- INSERTI u tablicu driverspoints
-------------------------------------
INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (290,2);
-----------
INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (264,3);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (224,4);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (196,5);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (192,6);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (192,7);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (164,8);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (161,9);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (156,10);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (142,11);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (139,12);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (138,13);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (137,14);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (129,15);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (112,16);
    
INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (112,17);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (94,18);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (93,19);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (83,20);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (82,21);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (76,22);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (71,23);
    
INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (68,24);
    
INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (66,25);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (63,26);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (63,27);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (62,28);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (57,29);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (50,30);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (44,31);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (33,32);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (30,33);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (26,34);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (20,35);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (12,36);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (12,37);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (7,38);

INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (6,39);
    
INSERT INTO DRIVERSPOINTS
    (points,driversid)
VALUES
    (1,40);
------------------------------------
--UPDATE za tablicu DRIVERSPOINTS
------------------------------------
UPDATE DRIVERSPOINTS
SET POINTS = POINTS + 25
WHERE POINTS <=50;

UPDATE DRIVERSPOINTS
SET POINTS = POINTS - 25
WHERE POINTS >=120;

UPDATE DRIVERSPOINTS
SET POINTS = POINTS + 60
WHERE POINTS <30;
--select * from DRIVERSPOINTS;

--
DELETE FROM DRIVERSPOINTS
WHERE POINTS <20;

DELETE FROM DRIVERSPOINTS
WHERE POINTS =57;


------------------------------------
--INSERT u tablicu AMpoints
------------------------------------
INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (152,15);
-------------------------------
INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (144,16);
    
INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (132,18);
  
INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (126,22);

INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (112,24);

INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (106,28);

INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (103,26);

INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (93,25);

INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (59,34);

INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (53,31);

INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (45,36);

INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (32,35);

INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (18,37);

INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (18,38);

INSERT INTO AMPOINTS
    (points,driversid)
VALUES
    (8,39);
----------------------------------------------
-- UPDATE ZA TABLICU AMPOINTS
----------------------------------------------
UPDATE AMPOINTS
SET POINTS = POINTS + 20
where POINTS<30;

UPDATE AMPOINTS
SET POINTS = POINTS - 10
where POINTS<100;

UPDATE AMPOINTS
SET POINTS = POINTS - 15
where POINTS>110;

--select * from AMPOINTS;
----------------------------------------------
--DELETE za tablicu AMPOINTS
--------------------------------------------
DELETE FROM AMPOINTS
WHERE POINTS <=30;
----------------------------------------------
--popunjavanje tablice Races
---------------------------------------------
INSERT INTO RACES
    (name, datum,trackid)
VALUES
    ('Zolder GP',SYSDATE,18);
    
INSERT INTO RACES
    (name, datum,trackid)
VALUES
    ('HUNGARIAN GP',SYSDATE,4);

INSERT INTO RACES
    (name, datum,trackid)
VALUES
    ('French GP',SYSDATE,12);

INSERT INTO RACES
    (name, datum,trackid)
VALUES
    ('US GP',SYSDATE,6);
    
INSERT INTO RACES
    (name, datum,trackid)
VALUES
    ('UK GP',SYSDATE,13);
    
INSERT INTO RACES
    (name, datum,trackid)
VALUES
    ('AUS GP',SYSDATE,9);

INSERT INTO RACES
    (name, datum,trackid)
VALUES
    ('GERMAN GP',SYSDATE,10);

INSERT INTO RACES
    (name, datum,trackid)
VALUES
    ('MISANO GP',SYSDATE,7);

INSERT INTO RACES
    (name, datum,trackid)
VALUES
    ('SOUTH AFRICA GP',SYSDATE,5);
    
INSERT INTO RACES
    (name, datum,trackid)
VALUES
    ('ITALIAN GP',SYSDATE,8);

INSERT INTO RACES
    (name, datum,trackid)
VALUES
    ('BRANDS GP',SYSDATE,2);

INSERT INTO RACES
    (name, datum,trackid)
VALUES
    ('JAPAN GP',SYSDATE,16);
    
INSERT INTO RACES
    (name, datum,trackid)
VALUES
    ('SPANISH GP',SYSDATE,1);

INSERT INTO RACES
    (name, datum,trackid)
VALUES
    ('BELGIAN GP',SYSDATE,15);
 
 
 
-------------------------------
--UPDATE tablice RACES
-------------------------------
UPDATE RACES
SET DATUM = '16-04-2021'
WHERE ID=1;

UPDATE RACES
SET DATUM = '23-04-2021'
WHERE ID=2;

UPDATE RACES
SET DATUM = '30-04-2021'
WHERE ID=3;

UPDATE RACES
SET DATUM = '07-05-2021'
WHERE ID=4;

UPDATE RACES
SET DATUM = '14-05-2021'
WHERE ID=5;

UPDATE RACES
SET DATUM = '21-05-2021'
WHERE ID=6;

UPDATE RACES
SET DATUM = '28-05-2021'
WHERE ID=7;

UPDATE RACES
SET DATUM = '21-01-2021'
WHERE ID=8;

UPDATE RACES
SET DATUM = '06-02-2021'
WHERE ID=9;

UPDATE RACES
SET DATUM = '12-02-2021'
WHERE ID=10;

UPDATE RACES
SET DATUM = '19-02-2021'
WHERE ID=11;

UPDATE RACES
SET DATUM = '26-02-2021'
WHERE ID=12;

UPDATE RACES
SET DATUM = '05-03-2021'
WHERE ID=13;

UPDATE RACES
SET DATUM = '13-03-2021'
WHERE ID=14;


--Primjeri za delete polja
DELETE FROM RACES
WHERE ID = 14;

DELETE FROM RACES
WHERE TRACKID = 18;

--select * from RACES;