--skripte za izradu paketa dohvat i podaci

--------------------------------------------------------------------------------
--dohvat.pks

create or replace NONEDITIONABLE PACKAGE DOHVAT AS 

  procedure p_get_drivers(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
  procedure p_get_teams(in_json in json_object_t, out_json out json_object_t);
  procedure p_get_tracks(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);

END DOHVAT;
--------------------------------------------------------------------------------
--dohvat.pkb

create or replace NONEDITIONABLE PACKAGE BODY DOHVAT AS
e_iznimka exception;
------------------------------------------------------------------------------------
--get_drivers
procedure p_get_drivers(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_drivers json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);
    
    l_string := in_json.TO_STRING; 
    
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;
    
    FOR x IN (
            SELECT 
               json_object('ID' VALUE d.ID, 
                           'NAME' VALUE d.NAME,
                           'SURNAME' VALUE d.SURNAME,
                           'CLASS' VALUE d.CLASS) as izlaz
             FROM
                drivers d
                WHERE
                d.ID = nvl(l_id, d.ID)
            OFFSET 
                ((nvl(l_page,1)-1) * NVL(l_perpage,1)) ROWS 
            FETCH NEXT 
                NVL(l_perpage,1) ROWS ONLY
            )
        LOOP
            l_zivotinje.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;
        
    SELECT
      count(1)
    INTO
       l_count
       

    FROM 
       drivers
    where
        ID = nvl(l_id, ID) ;
        
    l_obj.put('count',l_count);
    l_obj.put('data',l_drivers);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       l_obj.put('h_message', 'Gre�ka u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    
END p_get_drivers;
-----------------------------------------------------------------------------------------------------------
--races
procedure p_get_races(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_races json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);
    
    l_string := in_json.TO_STRING; 
    
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;
    
    FOR x IN (
            SELECT 
               json_object('ID' VALUE ID, 
                           'NAME' VALUE NAME,
                           'DATUM' VALUE CAR,
                           'TRACKID' VALUE TRACKID) as izlaz
             FROM
                races
             WHERE
                ID = nvl(l_id, ID) 
            OFFSET 
                ((nvl(l_page,1)-1) * NVL(l_perpage,1)) ROWS 
            FETCH NEXT 
                NVL(l_perpage,1) ROWS ONLY
            )
        LOOP
            l_races.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;
        
    SELECT
      count(1)
    INTO
       l_count
    FROM 
       races
    where
        ID = nvl(l_id, ID) ;
        
    l_obj.put('count',l_count);
    l_obj.put('data',l_races);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       l_obj.put('h_message', 'Gre�ka u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    
END p_get_races;
-----------------------------------------------------------------------------------------------------------
--teams
procedure p_get_teams(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
  l_obj JSON_OBJECT_T;
  l_vrste json_array_t :=JSON_ARRAY_T('[]');
  l_count number;
  l_id number;
  l_string varchar2(1000);
  l_search varchar2(100);
  l_page number; 
  l_perpage number; 
  l_teams.append varchar2(1000);
 BEGIN
    l_obj := JSON_OBJECT_T(in_json);
    
    l_string := in_json.TO_STRING; 
    
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.search'),
        JSON_VALUE(l_string, '$.page' ),
        JSON_VALUE(l_string, '$.perpage' )
    INTO
        l_id,
        l_search,
        l_page,
        l_perpage
    FROM 
       dual;
    
    FOR x IN (
            SELECT 
               json_object('ID' VALUE ID, 
                           'NAME' VALUE NAME,
                           'CAR' VALUE CAR,
                           'DRIVER1' VALUE DRIVER1,
                           'DRIVER2' VALUE DRIVER2) as izlaz
             FROM
                teams
             WHERE
                ID = nvl(l_id, ID) 
            OFFSET 
                ((nvl(l_page,1)-1) * NVL(l_perpage,1)) ROWS 
            FETCH NEXT 
                NVL(l_perpage,1) ROWS ONLY
            )
        LOOP
            l_teams.append(JSON_OBJECT_T(x.izlaz));
        END LOOP;
        
    SELECT
      count(1)
    INTO
       l_count
    FROM 
       teams
    where
        ID = nvl(l_id, ID) ;
        
    l_obj.put('count',l_count);
    l_obj.put('data',l_teams);
    out_json := l_obj;
 EXCEPTION
   WHEN OTHERS THEN
       l_obj.put('h_message', 'Gre�ka u obradi podataka');
       l_obj.put('h_errcode', 99);
       ROLLBACK;    
END p_get_teams;

END DOHVAT;
--------------------------------------------------------------------------------
--podaci.pks

create or replace NONEDITIONABLE PACKAGE PODACI AS 

 procedure p_podaci_drivers(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
 procedure p_podaci_races(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
 procedure p_podaci_teams(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
 

END PODACI;
--------------------------------------------------------------------------------
--podaci.pkb

create or replace NONEDITIONABLE PACKAGE BODY PODACI AS
e_iznimka exception;
--podaci drivers
-----------------------------------------------------------------------------------------
  procedure p_podaci_drivers(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
      l_obj JSON_OBJECT_T;
      l_drivers drives%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
      l_action varchar2(10);
  begin

     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;

     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.NAME'),
        JSON_VALUE(l_string, '$.SURNAME' ),
        JSON_VALUE(l_string, '$.CLASS' )
        
    INTO
        l_drivers.id,
        l_drivers.NAME,
        l_drivers.SURNAME,
        l_drivers.CLASS,
        l_action
    FROM 
       dual; 

    --FE kontrole
    if (nvl(l_action, ' ') = ' ') then
        if (filter.f_check_drivers(l_obj, out_json)) then
           raise e_iznimka; 
        end if;
        if(biznis.f_biznis_check_drivers(l_obj, out_json)) then
            raise e_iznimka;
        end if;
    end if;

    if (l_zivotinje.id is null) then
        begin
           insert into drivers (NAME, SURNAME, CLASS) values
             (l_drivers.NAME, l_drivers.SURNAME,
              l_drivers.CLASS);
           commit;

           l_obj.put('h_message', 'Uspje�no ste unijeli vozaca'); 
           l_obj.put('h_errcode', 0);
           out_json := l_obj;

        exception
           when others then 
               rollback;
               raise;
        end;
    else
       if (nvl(l_action, ' ') = 'delete') then
           begin
               delete drivers where id = l_drivers.id;
               commit;    

               l_obj.put('h_message', 'Uspje�no ste obrisali vozaca'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   rollback;
                   raise;
            end;

       else

           begin
               update drivers 
                  set NAME = l_drivers.NAME,
                      SURNAME = l_drivers.SURNAME,
                      CLASS = l_drivers.CLASS 
               where
                  id = l_drivers.id;
               commit;    

               l_obj.put('h_message', 'Uspje�no ste promijenili vozaca'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   rollback;
                   raise;
            end;
       end if;     
    end if;


  exception
     when e_iznimka then
        out_json := l_obj; 
     when others then
        l_obj.put('h_message', dbms_utility.format_error_backtrace || SQLERRM); 
        l_obj.put('h_errcode', 101);
        out_json := l_obj;
  END p_podaci_drivers;

  -------------------------------------------------------------------------------------------------------------------
  --podaci races
  procedure p_podaci_races(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
      l_obj JSON_OBJECT_T;
      l_races races%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
      l_action varchar2(10);
  begin

     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;

     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.NAME'),
        TO_DATE(JSON_VALUE(l_string,'$.DATUM'),'DD/MM/YYYY'),
        JSON_VALUE(l_string, '$.TRACKID')
        
    INTO
        l_races.id,
        l_races.NAME,
        l_races.DATUM,
        l_races.TRACKID,
        l_action
    FROM 
       dual; 

    --FE kontrole
    if (nvl(l_action, ' ') = ' ') then
        if (filter.f_check_races(l_obj, out_json)) then
           raise e_iznimka; 
        end if;
        if(biznis.f_biznis_check_races(l_obj, out_json)) then
            raise e_iznimka;
        end if;
    end if;

    if (l_radnici.id is null) then
        begin
           insert into races (NAME, DATUM, TRACKID) values
             (l_races.NAME, l_races.DATUM, l_races.TRACKID);
           commit;

           l_obj.put('h_message', 'Uspje�no ste unijeli utrku'); 
           l_obj.put('h_errcode', 0);
           out_json := l_obj;

        exception
           when others then 
               rollback;
               raise;
        end;
    else
       if (nvl(l_action, ' ') = 'delete') then
           begin
               delete races where id = l_races.id;
               commit;    

               l_obj.put('h_message', 'Uspje�no ste obrisali utrku'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   rollback;
                   raise;
            end;

       else

           begin
               update races 
                  set NAME = l_races.NAME,
                      DATUM = l_races.DATUM,
                      TRACKID = l_races.TRACKID,
                
                  id = l_radnici.id;
               commit;    

               l_obj.put('h_message', 'Uspje�no ste promijenili utrku'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   rollback;
                   raise;
            end;
       end if;     
    end if;


  exception
     when e_iznimka then
        out_json := l_obj; 
     when others then
        l_obj.put('h_message', dbms_utility.format_error_backtrace || SQLERRM); 
        l_obj.put('h_errcode', 101);
        out_json := l_obj;
  END p_podaci_races;
-------------------------------------------------------------------------------------------------------------------
--podaci teams
  procedure p_podaci_teams(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
      l_obj JSON_OBJECT_T;
      l_teams teams%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
      l_action varchar2(10);
  begin

     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;

     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.NAME'),
        JSON_VALUE(l_string, '$.CAR'),
        JSON_VALUE(l_string, '$.DRIVER1'),
        JSON_VALUE(l_string, '$.DRIVER2'),
        JSON_VALUE(l_string, '$.ACTION')
    INTO
        l_teams.id,
        l_teams.NAME,
        l_teams.CAR,
        l_teams.DRIVER1,
        l_teams.DRIVER2,
        l_action
    FROM 
       dual; 

    --FE kontrole
    if (nvl(l_action, ' ') = ' ') then
        if (filter.f_check_teams(l_obj, out_json)) then
           raise e_iznimka; 
        end if;
        if(biznis.f_biznis_check_teams(l_obj, out_json)) then
            raise e_iznimka;
        end if;
    end if;

    if (l_teams.id is null) then
        begin
           insert into teams (NAME, CAR, DRIVER1, DRIVER2) values
             (l_teams.NAME, l_teams.CAR, l_teams.DRIVER1, l_teams.DRIVER2);
           commit;

           l_obj.put('h_message', 'Uspje�no ste unijeli team'); 
           l_obj.put('h_errcode', 0);
           out_json := l_obj;

        exception
           when others then 
               rollback;
               raise;
        end;
    else
       if (nvl(l_action, ' ') = 'delete') then
           begin
               delete teams where id = l_teams.id;
               commit;    

               l_obj.put('h_message', 'Uspje�no ste obrisali team'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   rollback;
                   raise;
            end;

       else

           begin
               update teams
                  set NAME = l_teams.NAME,
                      CAR = l_teams.CAR,
                      DRIVER1 = l_teams.DRIVER1,
                      DRIVER2 = l_teams.DRIVER2
               where
                  id = l_teams.id;
               commit;    

               l_obj.put('h_message', 'Uspje�no ste promijenili team'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   rollback;
                   raise;
            end;
       end if;     
    end if;


  exception
     when e_iznimka then
        out_json := l_obj; 
     when others then
        l_obj.put('h_message', dbms_utility.format_error_backtrace || SQLERRM); 
        l_obj.put('h_errcode', 101);
        out_json := l_obj;
  END p_podaci_teams;
-------------------------------------------------------------------------------------------------------------------

END PODACI;
