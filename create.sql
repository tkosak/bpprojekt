drop table AMPoints;
drop table TeamsPoints;
drop table DriversPoints;
drop table Teams;
drop table Tracks;
drop sequence Tracks_ID_Seq;
drop table Drivers;
drop sequence Drivers_ID_Seq;
drop trigger BI_Drivers_ID;




CREATE TABLE AMPoints (
	Points NUMBER(4, 0) NOT NULL,
	DriversID NUMBER(4, 0) NOT NULL);

CREATE TABLE TeamsPoints (
	Points NUMBER(4, 0) NOT NULL,
	TeamsID NUMBER(4, 0) NOT NULL);

CREATE TABLE DriversPoints (
	Points NUMBER(4, 0) NOT NULL,
	DriversID NUMBER(4, 0) NOT NULL);


CREATE TABLE Teams (
	ID NUMBER(8, 0) NOT NULL,
	Name VARCHAR2(40) NOT NULL,
	Car VARCHAR2(40) NOT NULL,
	Driver1 VARCHAR2(40) NOT NULL,
	Driver2 VARCHAR2(40),
	Driver3 VARCHAR2(40),
	constraint TEAMS_PK PRIMARY KEY (ID));

-----------------------------------
--Ispravak , mjenjam drivere iz varchar u number jer su foreign keyevi iz tablice drivers
ALTER TABLE Teams
MODIFY Driver1 number(4);

ALTER TABLE Teams
MODIFY Driver2 number(4);

ALTER TABLE Teams
MODIFY Driver3 number(4);
-----------------------------------
CREATE sequence TEAMS_ID_SEQ;

CREATE trigger BI_TEAMS_ID
  before insert on Teams
  for each row
begin
  select TEAMS_ID_SEQ.nextval into :NEW.ID from dual;
end;


CREATE TABLE Tracks (
	ID NUMBER(8, 0) NOT NULL,
	Name VARCHAR2(40) NOT NULL,
	Country VARCHAR2(40) NOT NULL,
	Lenght NUMBER(10, 0),
	constraint TRACKS_PK PRIMARY KEY (ID));

CREATE sequence TRACKS_ID_SEQ;

CREATE trigger BI_TRACKS_ID
  before insert on Tracks
  for each row
begin
  select TRACKS_ID_SEQ.nextval into :NEW.ID from dual;
end;


CREATE TABLE Drivers (
	ID NUMBER(8, 0) NOT NULL,
	Name VARCHAR2(40) NOT NULL,
	Surname VARCHAR2(40) NOT NULL,
	Class VARCHAR2(20) NOT NULL,
	constraint DRIVERS_PK PRIMARY KEY (ID));

CREATE sequence DRIVERS_ID_SEQ;

CREATE trigger BI_DRIVERS_ID
  before insert on Drivers
  for each row
begin
  select DRIVERS_ID_SEQ.nextval into :NEW.ID from dual;
end;


/

CREATE TABLE Races (
	ID NUMBER(8, 0) NOT NULL,
	Name VARCHAR2(40) NOT NULL,
	Datum DATE NOT NULL,
	TrackID NUMBER(8, 0) NOT NULL,
	
	constraint RACES_PK PRIMARY KEY (ID));

CREATE sequence RACES_ID_SEQ;

CREATE trigger BI_RACES_ID
  before insert on Races
  for each row
begin
  select RACES_ID_SEQ.nextval into :NEW.ID from dual;
end;


ALTER TABLE Races ADD CONSTRAINT Races_fk0 FOREIGN KEY (TrackID) REFERENCES Tracks(ID);




ALTER TABLE Teams ADD CONSTRAINT Teams_fk0 FOREIGN KEY (Driver1) REFERENCES Drivers(ID);
ALTER TABLE Teams ADD CONSTRAINT Teams_fk1 FOREIGN KEY (Driver2) REFERENCES Drivers(ID);
ALTER TABLE Teams ADD CONSTRAINT Teams_fk2 FOREIGN KEY (Driver3) REFERENCES Drivers(ID);

ALTER TABLE DriversPoints ADD CONSTRAINT DriversPoints_fk0 FOREIGN KEY (DriversID) REFERENCES Drivers(ID);

ALTER TABLE TeamsPoints ADD CONSTRAINT TeamsPoints_fk0 FOREIGN KEY (TeamsID) REFERENCES Teams(ID);

ALTER TABLE AMPoints ADD CONSTRAINT AMPoints_fk0 FOREIGN KEY (DriversID) REFERENCES Drivers(ID);
