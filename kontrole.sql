--skripte za izradu router i bizinis paketa

--------------------------------------------------------------------------------
--biznis.pks

create or replace NONEDITIONABLE PACKAGE ROUTER AS 
 e_iznimka exception;
    
 procedure p_main(p_in in varchar2, p_out out varchar2);

END ROUTER;

--------------------------------------------------------------------------------
--router.pkb
create or replace NONEDITIONABLE PACKAGE BODY ROUTER AS

  procedure p_main(p_in in varchar2, p_out out varchar2) AS
    l_obj JSON_OBJECT_T;
    l_procedura varchar2(40);
  BEGIN
    l_obj := JSON_OBJECT_T(p_in);

    SELECT
        JSON_VALUE(p_in, '$.procedura' RETURNING VARCHAR2)
    INTO
        l_procedura
    FROM DUAL;

    CASE l_procedura
    WHEN 'p_get_drivers' THEN
        dohvat.p_get_drivers(JSON_OBJECT_T(p_in), l_obj);
    WHEN 'p_get_teams' THEN
        dohvat.p_get_teams(JSON_OBJECT_T(p_in), l_obj); 
    WHEN 'p_get_tracks' THEN
        dohvat.p_get_tracks(JSON_OBJECT_T(p_in), l_obj); 
    WHEN 'p_save_drivers' THEN
        spremi.p_save_drivers(JSON_OBJECT_T(p_in), l_obj);     
    ELSE
        l_obj.put('h_message', ' Nepoznata metoda ' || l_procedura);
        l_obj.put('h_errcode', 997);
    END CASE;
    p_out := l_obj.TO_STRING;
  END p_main;
END ROUTER;

--------------------------------------------------------------------------------
--biznis.pks

create or replace NONEDITIONABLE PACKAGE BIZNIS AS 

  function f_biznis_check_drivers(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) return boolean;
  function f_biznis_check_races(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) return boolean;
  function f_biznis_check_teams(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) return boolean;
  

END BIZNIS;

--------------------------------------------------------------------------------
--biznis.pkb

create or replace NONEDITIONABLE PACKAGE BODY BIZNIS AS
e_iznimka exception;

--drivers
  function f_biznis_check_drivers(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) return boolean AS
      l_obj JSON_OBJECT_T;
      l_drivers drivers%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
      broj number;
  BEGIN
     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;

     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.NAME'),
        JSON_VALUE(l_string, '$.SURNAME' ),
        JSON_VALUE(l_string, '$.CLASS' ),
    INTO
        l_drivers.id,
        l_drivers.NAME,
        l_drivers.SURNAME,
        l_drivers.CLASS,
        
    FROM 
       dual; 

    if(l_drivers.CLASS != 'PRO' || l_drivers.CLASS != 'AM') then
        l_obj.put('h_message', 'Klasa moze biti samo PRO ili AM');
        l_obj.put('h_errcode', 105);
        raise e_iznimka;
    end if;

  EXCEPTION
     WHEN E_IZNIMKA THEN
        return true;
     WHEN OTHERS THEN
        l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka!'); 
        l_obj.put('h_errcode', 109);
        out_json := l_obj;
        return true;
  END f_biznis_check_drivers;

-------------------------------------------------------------------------------------------------------------------
--radnici
  function f_biznis_check_races(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) return boolean AS
      l_obj JSON_OBJECT_T;
      l_races races%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
      broj number;
  BEGIN
     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;

     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.NAME'),
        TO_DATE(JSON_VALUE(l_string, '$.DATUM'),'DD/MM/YYYY'),
        JSON_VALUE(l_string, '$.TRACKID')
    INTO
        l_radnici.id,
        l_radnici.NAME,
        l_radnici.DATUM,
        l_radnici.TRACKID
    FROM 
       dual; 

    if (l_radnici.SPOL < 0 or l_radnici.SPOL > 1) then   
       l_obj.put('h_message', 'Molimo unesite ispravan spol'); 
       l_obj.put('h_errcode', 113);
       raise e_iznimka;
    end if;

    SELECT
        count(1) into broj
    FROM
        poslovi
    WHERE
        id = l_radnici.IDPOSLA;

    if(broj = 0) then
        l_obj.put('h_message', 'Unesen pogresan ID posla');
        l_obj.put('h_errcode',106);
        raise e_iznimka;
    end if;


    out_json := l_obj;
    return false;

  EXCEPTION
     WHEN E_IZNIMKA THEN
        return true;
     WHEN OTHERS THEN
        l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka'); 
        l_obj.put('h_errcode', 114);
        out_json := l_obj;
        return true;
  END f_biznis_check_radnici;

  -------------------------------------------------------------------------------------------------------------------
--teams
  function f_biznis_check_teams(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) return boolean AS
      l_obj JSON_OBJECT_T;
      l_teams teams%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
  BEGIN
     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;

     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.NAME'),
        JSON_VALUE(l_string, '$.CAR'),
        JSON_VALUE(l_string, '$.DRIVER1'),
        JSON_VALUE(l_string, '$.DRIVER2')
    INTO
        l_vrste.id,
        l_vrste.NAME,
        l_vrste.CAR,
        l_vrste.DRIVER1,
        l_vrste.DRIVER2
    FROM 
       dual; 

    out_json := l_obj;
    return false;

  EXCEPTION
     WHEN E_IZNIMKA THEN
        return true;
     WHEN OTHERS THEN
        l_obj.put('h_message', 'Dogodila se gre�ka u obradi podataka'); 
        l_obj.put('h_errcode', 119);
        out_json := l_obj;
        return true;
  END f_biznis_check_teams;

  -------------------------------------------------------------------------------------------------------------------


END BIZNIS;
